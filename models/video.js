module.exports = function(sequelize, DataTypes) {
	var Video = sequelize.define('videos', {
		url: DataTypes.STRING,
		status: DataTypes.INTEGER
	});
	return Video;
}