$(document).ready(function() {
    splitOptions();    
    $('#folder').click(function(event) {
        chrome.downloads.showDefaultFolder();
    });
    $('#example4').progress('reset');
    $('.btn').click(function(event) {
       
        //$('#example4').progress({percent: e});
    });
});

function splitOptions() {
    var videockeck1 = $('#freq_1');
    var videockeck2 = $('#freq_2');
    var splitsame = $('#same');
    var splitmin = $('#min');
    $('#same').attr('disabled', 'true')
    $('#min').attr('disabled', 'true')
    $('.sameop').hide();
    $('.minop').hide();
    $('.same').click(function(event) {
        $('.sameop').toggle();
    });
    videockeck1.click(function(event) {
        splitsame.removeAttr('disabled');
        splitmin.removeAttr('disabled');
        splitsame.attr('disabled', 'true');
        splitmin.attr('disabled', 'true');
        splitsame.prop('checked', false);
        splitmin.prop('checked', false);
        $('.sameop').hide();
        $('.minop').hide();
    });
    videockeck2.click(function(event) {
        splitsame.removeAttr('disabled');
        splitmin.removeAttr('disabled');
    });
    splitsame.click(function(event) {
        $('.sameop').show();
        $('.minop').hide();

    });
    splitmin.click(function(event) {
        $('.sameop').hide();
        $('.minop').show();
    });

}