const {
    exec
} = require('child_process');
var models = require('./models');
var fs = require('fs');
var youtubedl = require('youtube-dl');
var userhome = require('userhome');
var path = userhome('Downloads') + '/';
var amqp = require('amqplib/callback_api');
var replaceall = require("replaceall");
var stractname;
var filename;
var criteria = function(req) {
    return {
        where: {
            id: req.params.id
        }
    };
}

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        var q = 'channel_video_transcoder';
        ch.assertQueue(q, {
            durable: false
        });
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
        ch.consume(q, function(msg) {
            console.log(" [x] Received %s", msg.content.toString());
            var jsonObject = JSON.parse(msg.content.toString());
            var youlink = jsonObject.url.indexOf('www.youtube.com');
            update_video_status(jsonObject, youlink > -1 ? 2 : 4);            
        }, {
            noAck: true
        });
    });
});

function update_video_status(jsonObject, status) {
    models.videos.update({
        status: status
    }, {
        where: {
            id: jsonObject.id
        }
    }).then(
        updated => {
            if (status == 2) {
                start_video_transcoder(jsonObject);
                return;
            }
        }
    );
}

function start_video_transcoder(jsonObject) {
    if (jsonObject.file == 'video') {
        downloadVideo(jsonObject);
        return;
    }
    downloadOnlyAudio(jsonObject);
    return;

}

function downloadVideo(jsonObject) {
    // var downloaded = 0;
    // var video = youtubedl(jsonObject.url, ['--format=18'], {
    //     start: downloaded,
    //     cwd: __dirname
    // });
    // video.on('info', function(info) {
    //     var videoName = info.title.replace('|', '').toString('ascii');
    //     var total = info.size + downloaded;
    //     console.log('size: ' + total);
    //     videoName = replaceall('"', '', videoName);
    //     videoName = replaceall('/', '', videoName);
    //     video.pipe(fs.createWriteStream(path + videoName));
    //     video.on('complete', function complete(info) {
    //         'use strict';
    //         console.log('filename: ' + info._filename + ' already downloaded.');
    //     });
    //     video.on('end', function() {
    //         update_video_status(jsonObject, 3);
    //         console.log('finished downloading!');
    //     });

    // });
//youtube-dl -f 22 -o 'home/christopher/Downloads/%(title)s.%(ext)s' "https://www.youtube.com/watch?v=nFnoLpKrTyE"^C    
    exec("youtube-dl -f 22 -o '" + path + "%(title)s.%(ext)s' " + jsonObject.url, (err, stdout, stderr) => {
        if (err) {
            console.log("exec error:" + err);
        }
        update_video_status(jsonObject, 3);
        console.log('download success, updated');
    });
}

function downloadOnlyAudio(jsonObject) {

    var audio = new Promise((resolve, reject) => {
        exec("youtube-dl -o '" + path + "%(title)s.%(ext)s" + "' --extract-audio --audio-format mp3 " + jsonObject.url, (err, stdout, stderr) => {
            if (err) {
                console.log("exec error:" + err);
                reject(err);
                return;
            }
            resolve(stdout);
        });
    });
    audio.then(success => {
        var getfilename = new Promise((resolve, reject) => {
            exec("youtube-dl --get-filename -o'" + path + "%(title)s.%(ext)s" + "' --extract-audio --audio-format mp3 " + jsonObject.url, (err, stdout, stderr) => {
                if (err) {
                    console.log("exec error:" + err);
                    reject(err);
                    return;
                }
                stractname = stdout.substring(0, stdout.length - 5).concat('mp3');
                resolve(stractname);
            });
        });
        getfilename.then(getsuccess => {
            if (jsonObject.audiop == 'audioparts') {
                audioBySamePart(jsonObject);
                return;
            }
            if (jsonObject.audiop == 'audiominutes') {
                audioByMinute(jsonObject, stractname);
                return;
            }
            update_video_status(jsonObject, 3);
        });
    }, fail => {
        contains(fail);
    });
}

function audioBySamePart(jsonObject) {
    console.log('audio by same parts ' + jsonObject.audiop);

}

function audioByMinute(jsonObject, audiopath) {

    exec("ffmpeg  -i '" + audiopath + "' -f segment -segment_time " + jsonObject.minutes + " -c copy " + path + "audio%d.mp3", (err, stdout, stderr) => {
        if (err) {
            console.error('exec error:' + err);
            return;
        }
        console.log("Splited");
        // audiopath = replaceall(' ', '',audiopath);
        // audiopath = replace(/[&\/\\#,+()$~%.'":*?*!*¡*¿<>{}]/g, '',audiopath);
        // console.log(audiopath);
        // // exec("rm -r "+ audiopath, (err, stdout, stderr) => {
        // //     if (err) {
        // //         console.error('exec error:' + err);
        // //         return;
        // //     }
        // //     console.log("remove success " + stdout);

        // // })
    });
}