var express = require('express');
var app = express();
var router = express.Router();
var models = require('../models');
var amqp = require('amqplib/callback_api');
var cors = require('cors');
router.all('*', cors());
var criteria = function(req) {
    return {
        where: {
            id: req.params.id
        }
    };
}

router.get('/:id', function(req, res, next) {
    console.log(req);
    var video = models.videos.findOne(criteria(req));
    res.format({
        json: function() {
            video.then(video => {
                var response = {
                    id: video.id,
                    status: video.status,
                    message: video.status == 1 ? 'in queue' : video.status == 2 ? 'In Process' : video.status == 3 ? 'Finish' : 'Fail'
                }
                res.json(response);
            });
        },
    })
});

router.post('/', function(req, res, next) {
    var video = models.videos.create({
        url: req.body.url
    });
    video.then(success => {
        req.body.id = success.id;
        amqp.connect('amqp://localhost', function(err, conn) {
            conn.createChannel(function(err, ch) {
                var channel = 'channel_video_transcoder';
                var msg = JSON.stringify(req.body);
                ch.assertQueue(channel , {
                    durable: false
                });
                ch.sendToQueue(channel , new Buffer(msg));
                console.log(" [x] Sent %s", msg);
            }); 
        });
        res.json({
            status: "ok",
            id: success.id
        });
    }, f => {
        console.log("se espichó" + f);
    });
});
module.exports = router;
